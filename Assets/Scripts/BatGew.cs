﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatGew : MonoBehaviour
{

    public float constantSpeed;
    // Use this for initialization
    Rigidbody2D GewBody;
    Vector3 velocity;
    Vector3 currDir;

    void Start()
    {
        GewBody = GetComponent<Rigidbody2D>();
        StartCoroutine(WaitAndDestroy(7f));
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        GewBody.velocity = constantSpeed * (GewBody.velocity.normalized);
     

    }

    IEnumerator WaitAndDestroy(float delay = 0.0f)
    {
        yield return new WaitForSeconds(delay);
        Destroy(this.gameObject);
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Ontrigger");
        if (col.collider.tag == "Wall")
        {
            // It is object B

            Destroy(this.gameObject);

        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float constantSpeed;
    // Use this for initialization
    Rigidbody2D bulletBody;
    Vector3 velocity;
    Vector3 currDir;
    public AudioClip BulletBounce;
    public AudioSource source;

    void Start () {
        bulletBody = GetComponent<Rigidbody2D>();
        source = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        currDir = new Vector3(transform.position.x, transform.position.y, 0);
        bulletBody.velocity = constantSpeed * (bulletBody.velocity.normalized);
        velocity = bulletBody.velocity;
        
    }

    void OnCollisionEnter2D(Collision2D col ) //When we run into something
    {
        if (col.collider.tag == "Wall")
        {
            source.clip = BulletBounce;
            source.Play();
            Vector2 surfaceNormal = col.contacts[0].normal;
            bulletBody.velocity = Vector2.Reflect(velocity, surfaceNormal);
            var direction = bulletBody.velocity;
            direction.Normalize();
            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            bulletBody.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }
}

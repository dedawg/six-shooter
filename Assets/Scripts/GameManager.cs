﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    // Use this for initialization
    GameObject Cowber;
    public Transform[] spawnPoints;
    public Transform DogSpawnPoint;
    public Rigidbody2D SlugEnemy;
    public Rigidbody2D BatEnemy;
    public Rigidbody2D DogBuddy;
    public Rigidbody2D BatDogBuddy;
    public static int score = 0;
    public static bool ScoreBool;
    public static int HighScore = 0;
    public Text ScoreText;
    public int DifficultyScore;
   
    int batNumber = 0;
    public int scoreForDog;
    int amountOfDogs = 0;
    int spawnLevel = 1;
    void Start()
    {
        Cowber = GameObject.Find("Cowber");
        score = 0;
        // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        InvokeRepeating("Spawn", 5, 3);
        InvokeRepeating("AddOneK", 1, 1);
    }


    void Spawn()
    {

        for (int x = 0; x < spawnLevel; x++)
        {
            // If the player has no health left...
            if (Cowber == null)
            {
                // ... exit the function.
                return;
            }

            // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
            int spawnPointIndexBat = Random.Range(0, spawnPoints.Length);
            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            Debug.Log("SlugPoint" + spawnPoints[spawnPointIndex].position);
            Instantiate(SlugEnemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
            if (batNumber == 1)
            {
                Instantiate(BatEnemy, spawnPoints[spawnPointIndexBat].position, spawnPoints[spawnPointIndexBat].rotation);
                batNumber--;
            }
            else
            {
                batNumber++;
            }
        }
    }

    // Update is called once per frame
    void Update () {

        if(score/DifficultyScore >= spawnLevel)
        {
            spawnLevel++;
        }
        if (score / scoreForDog > amountOfDogs)
        {
            int dogChooser = Random.Range(0, 2);
            amountOfDogs++;
            if( dogChooser == 0)
            {
                Instantiate(DogBuddy, DogSpawnPoint.position, DogSpawnPoint.rotation);
            }
            else
            {
                Instantiate(BatDogBuddy, DogSpawnPoint.position, DogSpawnPoint.rotation);
            }
            
        }
        if(ScoreBool == true)
        {
            ScoreText.text = score.ToString();
            ScoreBool = false;
        }
		
	}

    void AddOneK()
    {
        score += 10;
        ScoreText.text = score.ToString();
    }

    public void playagain()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
        Time.timeScale = 1;
    }
    public void mainmenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        Time.timeScale = 1;
    }
}

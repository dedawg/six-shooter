﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slug : MonoBehaviour
{
    GameObject Cowber;
    public float speed;
    private Rigidbody2D slugBody;
    bool charging = false;
    Vector2 CowberSetPosition;
    public float chargeSpeed;
    public bool CoolDown;
    public Text PlusOneHundred;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    AudioSource source;
    public AudioClip deathClip;
    
    public bool alive = true;
    // Use this for initialization
    void Start () {
        Cowber = GameObject.Find("Cowber");
        slugBody = gameObject.GetComponent<Rigidbody2D>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        PlusOneHundred.enabled = false;

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (alive == true)
        {
            if (charging == false)
            {
                Vector2 direction = Cowber.transform.position - transform.position;
                if (direction.magnitude > 7 || CoolDown == true)
                {
                    direction.Normalize();
                    slugBody.velocity = speed * direction;



                    bool flipSprite = (spriteRenderer.flipX ? (slugBody.velocity.x < 0.01f) : (slugBody.velocity.x > 0.01f));
                    if (flipSprite)
                    {
                        spriteRenderer.flipX = !spriteRenderer.flipX;
                    }


                }
                else
                {
                    animator.SetInteger("Slug", 1);
                    charging = true;
                    CoolDown = true;
                    direction.Normalize();
                    CowberSetPosition = direction;
                    slugBody.velocity = Vector2.zero;
                    StartCoroutine(Charge(0.5f));

                }

            }
            else
            {

            }
        }
        

    }

    IEnumerator Charge(float delay = 0.0f)
    {
        if (alive == true)
        {


            yield return new WaitForSeconds(delay);
            if (alive == true)
            {


                animator.SetInteger("Slug", 2);
                yield return new WaitForSeconds(delay);
                Debug.Log("CHARGE!");
                if (alive == true)
                {


                    slugBody.velocity = chargeSpeed * CowberSetPosition;
                    animator.SetInteger("Slug", 3);
                    yield return new WaitForSeconds(0.6f);
                    if (alive == true)
                    {


                        charging = false;
                        animator.SetInteger("Slug", 0);
                        yield return new WaitForSeconds(2f);
                        CoolDown = false;
                    }
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Ontrigger");
        if (col.collider.tag == "Bullet" || col.collider.tag == "Doggo")
        {
            // It is object B
            source.clip = deathClip;
            source.Play();
            alive = false;
            slugBody.velocity = Vector2.zero;
            Destroy(GetComponent<Collider2D>());
            animator.SetInteger("Slug", 5);

            PlusOneHundred.enabled = true;
            var position = transform.position;
            position.y += 10;
            PlusOneHundred.transform.position = position;
            PlusOneHundred.text = "+100";
            
            GameManager.score += 10;
            GameManager.ScoreBool = true;
            Debug.Log("Score:" + GameManager.score);
            StartCoroutine(killSlug(2));
            
            
            
        }
        if (col.gameObject.tag == "Wall"|| col.gameObject.tag == "BatGew" || col.gameObject.tag == "Bat")
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
        }


    }
    IEnumerator killSlug(float delay = 0.0f)
    {
        yield return new WaitForSeconds(delay);

       
        Destroy(gameObject);
    }
}

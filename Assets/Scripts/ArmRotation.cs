﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour
{

    public int rotationOffset = 90;
    bool switched = false;
    public SpriteRenderer spriteRenderer;
    // Update is called once per frame
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        // subtracting the position of the player from the mouse position.
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        difference.Normalize(); // normalizing the vector. Meaning that all the sum of the vector will be equal to 1.

        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;  //find the angle in degrees
        
        if(rotZ >90 || rotZ < -90)
        {
           
                transform.localPosition = new Vector3(-0.62f, transform.localPosition.y, transform.localPosition.z);

            if (!spriteRenderer.flipY)
            {
                spriteRenderer.flipY = true;
            }
            
           
        }
        else
        {
            transform.localPosition = new Vector3(0.70f, transform.localPosition.y, transform.localPosition.z);
            if(spriteRenderer.flipY)
            {
                spriteRenderer.flipY = false;
            }
            
        }
       
        transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Cowber : MonoBehaviour {

    // Use this for initialization
    public float speed;             //Floating point variable to store the player's movement speed.
    public AudioClip BangClip;
    public AudioClip ReloadClip;
    public AudioClip OutOfAmmoClip;
    public AudioClip HeartClip;
    public AudioClip LostHeartClip;
    public AudioSource BangAudio;
    private Rigidbody2D rb2d;
    public Rigidbody2D bulletPrefab;
    public Rigidbody2D heartPrefab;
    int BulletCount = 0;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    public Canvas gameovercanvas;
    public List<Image> BulletImages;
    public List<Image> HeartImages;
    private bool alive = true;
    public Text HighScoreText;
    public int lives;
    public int NextLife;
    public Transform heartSpawn;
    int LivesGiven = 1;

    Vector3 originalCameraPosition;

    float shakeAmt = 0;

    public Camera mainCamera;

    void Start () {
        lives = 3;
        rb2d = GetComponent<Rigidbody2D>();
        BangAudio = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        originalCameraPosition = new Vector3(0, 0, -10);
      
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (GameManager.score / NextLife >= LivesGiven)
        {
            LivesGiven++;
            if (lives<3)
            {
                BangAudio.clip = HeartClip;
                BangAudio.Play();
                Debug.Log("FIRE!");
                var bullet = Instantiate(
               heartPrefab,
               heartSpawn.position,
               Quaternion.identity) as Rigidbody2D;
            }

        }


            Vector3 cursorInWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var facing = cursorInWorldPos - transform.position;

            //Store the current horizontal input in the float moveHorizontal.
            float moveHorizontal = Input.GetAxis("Horizontal");

            //Store the current vertical input in the float moveVertical.
            float moveVertical = Input.GetAxis("Vertical");

            //Use the two store floats to create a new Vector2 variable movement.
            Vector2 movement = new Vector2(moveHorizontal, moveVertical);

            bool flipSprite = (spriteRenderer.flipX ? (facing.x > 0.01f) : (facing.x < 0.01f));
            if (flipSprite)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
            }


            //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
            rb2d.velocity = (movement * speed);
            animator.SetFloat("Run", rb2d.velocity.x);

            if (Input.GetMouseButtonDown(0))
            {
                Fire();
            }
     
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Ontrigger");
        if (col.collider.tag == "Bullet")
        {
            // It is object B
            BangAudio.clip = ReloadClip;
            BangAudio.Play();
            Debug.Log("COllect Bullet");
            Destroy(col.gameObject);
            BulletCount--;

            var tempColor = BulletImages[BulletCount].color;
            tempColor.a = 1f;

            BulletImages[BulletCount].color = tempColor;
        }
        if(col.collider.tag == "Heart")
        {
            BangAudio.clip = HeartClip;
            BangAudio.Play();
            var tempColor = HeartImages[lives].color;
            tempColor.a = 1f;

            HeartImages[lives].color = tempColor;
            lives++;

            Destroy(col.gameObject);
        }
        if (col.collider.tag == "Enemy" || col.collider.tag == "Slug" || col.collider.tag == "BatGew")
        {
            BangAudio.clip = LostHeartClip;
            BangAudio.Play();
            if (lives == 0)
            {

                foreach (Transform child in gameObject.transform)
                {
                    Destroy(child.gameObject);
                }
                animator.SetBool("Death", true);
                alive = false;
                rb2d.bodyType = RigidbodyType2D.Static;
            }
            else
            {
                shakeAmt = col.relativeVelocity.magnitude * .0075f;
                InvokeRepeating("CameraShake", 0, .01f);
                Invoke("StopShaking", 0.3f);

                lives--;
                var tempColor = HeartImages[lives].color;
                tempColor.a = 0f;

                HeartImages[lives].color = tempColor;
                Destroy(col.gameObject);
            }
            
        }
        if (col.gameObject.tag == "Doggo" || col.gameObject.tag == "BatDoggo" || col.gameObject.tag == "Bat")
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
        }
    }
    public void GameOver()
    {
        
        Time.timeScale = 0;
        Debug.Log("GameOver");
        if(GameManager.score > GameManager.HighScore)
        {
            GameManager.HighScore = GameManager.score;
        }
        HighScoreText.text = "HighScore: " + GameManager.HighScore;
        gameovercanvas.enabled = true;
        Destroy(this.gameObject);
    }
    
    public void Fire()
    {
        if (BulletCount < 6)
        {


            var tempColor = BulletImages[BulletCount].color;
            tempColor.a = 0.3f;
           
            BulletImages[BulletCount].color = tempColor;
            
            BangAudio.clip = BangClip;
            BangAudio.Play();
            BulletCount++;
            Vector3 cursorInWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 direction = cursorInWorldPos - transform.GetChild(0).transform.position;
            direction.Normalize(); 
            Debug.Log("FIRE!");
            var bullet = Instantiate(
           bulletPrefab,
           transform.GetChild(0).transform.position + direction*3.2f,
           Quaternion.identity) as Rigidbody2D;



            bullet.velocity = direction * 2;
            //Debug.Log(cursorInWorldPos);

            float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            bullet.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
        else
        {
            BangAudio.clip = OutOfAmmoClip;
            BangAudio.Play();
            //trigger audio noise of no ammo :(
        }

    }

    void CameraShake()
    {
        if (shakeAmt > 0)
        {
            float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
            Vector3 pp = mainCamera.transform.position;
            pp.y += quakeAmt; // can also add to x and/or z
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
        mainCamera.transform.position = originalCameraPosition;
    }
}

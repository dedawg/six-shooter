﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doggo : MonoBehaviour
{
    GameObject Cowber;
    public float speed;
    Rigidbody2D dogBody;

    bool sit = true;
    GameObject SlugBody;
    bool hunting = false;

    public bool batDoggo;
    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public AudioClip bark;
    private AudioSource source;
    // Use this for initialization
    void Start()
    {
        
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        Cowber = GameObject.Find("Cowber");
        sit = true;
        dogBody = GetComponent<Rigidbody2D>();
        dogBody.bodyType = RigidbodyType2D.Static;
        StartCoroutine(Find());

        source = GetComponent<AudioSource>();
        source.clip = bark;
        source.Play();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (sit == false)
        {
            if (hunting == true)
            {
                if (SlugBody == null)
                {
                    hunting = false;
                    return;
                }

                Vector2 direction = SlugBody.transform.position - transform.position;

                direction.Normalize();
                dogBody.velocity = speed * direction;



            }
            else
            {
                dogBody.velocity = Vector2.zero;
            }
            animator.SetFloat("Run", dogBody.velocity.x);
                bool flipSprite = (spriteRenderer.flipX ? (dogBody.velocity.x > 0.01f) : (dogBody.velocity.x < 0.01f));
                if (flipSprite)
                {
                    spriteRenderer.flipX = !spriteRenderer.flipX;
                }
            
        }


    }

    IEnumerator Find()
    {
        if (sit == false)
        {
            int WaitTime = Random.Range(0, 5);
            yield return new WaitForSeconds(WaitTime);
            if (hunting == false)
            {
                if (batDoggo == true)
                {
                    SlugBody = GameObject.FindGameObjectWithTag("Bat");
                }
                else
                {
                    SlugBody = GameObject.FindGameObjectWithTag("Slug");
                }

                if (SlugBody != null)
                {

                    hunting = true;
                }
            }
            
        }
        yield return new WaitForSeconds(3);
            StartCoroutine(Find());
    }



    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Ontrigger");
        if (sit == false)
        {


            if (batDoggo == true)
            {
                
                if (col.collider.tag == "Bat")
                {
                    // It is object B
                    hunting = false;



                }
                if (col.gameObject.tag == "Slug")
                {
                    Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
                }

            }
            else
            {


                if (col.collider.tag == "Slug")
                {
                    // It is object B
                    hunting = false;



                }
                if (col.gameObject.tag == "Bat")
                {
                    Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
                }
            }
            if (col.gameObject.tag == "Doggo" || col.gameObject.tag == "BatDoggo" || col.gameObject.tag == "Bullet" || col.gameObject.tag == "BatGew")
            {
                Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
            }
        }
        else
        {
            if (col.collider.tag == "Cowber")
            {

                // It is object B
                dogBody.bodyType = RigidbodyType2D.Dynamic;
                sit = false;
              



            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    GameObject Cowber;
    public float speed;
    Rigidbody2D batBody;
    public AudioClip BatDeathClip;
    Vector2 CowberSetPosition;
    public Rigidbody2D BatGew;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    public AudioClip FireBall;
    public AudioSource source;
    bool alive = true;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        Cowber = GameObject.Find("Cowber");
        batBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        InvokeRepeating("Shoot", 1, 3);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (alive == true)
        {


            Vector2 direction = Cowber.transform.position - transform.position;

            bool flipSprite = (spriteRenderer.flipX ? (batBody.velocity.x < 0.01f) : (batBody.velocity.x > 0.01f));
            if (flipSprite)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
            }

            if (direction.magnitude > 6)
            {
                direction.Normalize();
                batBody.velocity = speed * direction;
            }
            else
            {
                batBody.velocity = Vector2.zero;
            }

        }
        else
        {
            batBody.velocity = Vector2.zero;
        }
       

    }

    void Shoot()
    {

        source.clip = FireBall;
        source.Play();
        animator.SetBool("Shoot", true);
        Vector3 direction = Cowber.transform.position - transform.position;
        direction.Normalize();
        Debug.Log("FIRE!");
        var bullet = Instantiate(
       BatGew,
       transform.position + direction,
       Quaternion.identity) as Rigidbody2D;



        bullet.velocity = direction * 2;
        //Debug.Log(cursorInWorldPos);

        float rot_z = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        bullet.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        

    }
    void AnimationEnded()
    {
        animator.SetBool("Shoot", false);
    }

 

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("Ontrigger");
        if (col.collider.tag == "Bullet" || col.collider.tag == "BatDoggo")
        {
            // It is object B
            CancelInvoke();
            GameManager.score += 20;
            GameManager.ScoreBool = true;
            animator.SetBool("Dead", true);
            source.clip = BatDeathClip;
            source.Play();
            alive = false;
            StartCoroutine(killBat(2));

        }
    }

    IEnumerator killBat(float delay = 0.0f)
    {
        yield return new WaitForSeconds(delay);


        Destroy(gameObject);
    }
}
